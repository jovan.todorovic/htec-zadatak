import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
// Redux
import { Provider } from 'react-redux';
import store from './store/index';
// Translation config
import './i18n';
import App from './App';
import './index.sass';

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store()}>
          <Suspense fallback="loading">
            <App />
          </Suspense>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
