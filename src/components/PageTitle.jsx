import React from 'react';
import styled from 'styled-components';
import './PageTitle.sass';
const Dot = styled.span`
        height: 10px;
        width: 10px;
        margin: 5px;
        background-color: black;
        border-radius: 50%;
        display: inline-block;
    `;

function PageTitle(props) {
    return (
        <h3 className='title'>
            <Dot/>
            {props.title}
        </h3>
    )
}

export default PageTitle;
