import React, {Component} from 'react';
// Translation
import {withTranslation} from "react-i18next";
// React components
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
// Components
import Article from "./Article";
// Styles
import './CategoryList.sass';
// Redux
import {connect} from "react-redux";
import CategoryTitle from "./CategoryTitle";

class CategoryList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            language: null,
            list: [],
            index: 0,
            limiter: 5
        };
        this.getArticleByCategory = this.getArticleByCategory.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.handleNext = this.handleNext.bind(this);
    }

    componentDidMount() {
        this.setLimiter();
        this.getArticleByCategory();
    }

    componentDidUpdate() {
        if (this.state.language !== this.props.i18n.language) {
            this.getArticleByCategory();
        }
    }

    setLimiter() {
        if(window.screen.width <= 1200) {
            this.setState({
                limiter: 3
            })
        }

        if(window.screen.width <= 768) {
            this.setState({
                limiter: 2
            })
        }

        if(window.screen.width <= 576) {
            this.setState({
                limiter: 1
            })
        }
    }

    handleNext(i, l) {
        this.setState({
            index: ++i,
            limiter: ++l
        });
    }

    handlePrev(i, l) {
        this.setState({
            index: --i,
            limiter: --l
        });
    }

    // Get articles for certain category
    getArticleByCategory() {
        let API_URL;
        const BASE_URL = process.env.REACT_APP_BASE_URL;
        const API_KEY = process.env.REACT_APP_API_KEY;
        const CATEGORY = '&category=' + this.props.category;
        API_URL = BASE_URL + 'top-headlines?country=' + this.props.i18n.language + CATEGORY + API_KEY;
        fetch(API_URL)
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    articles: data.articles,
                    list: data.articles.slice(this.index, this.state.limiter),
                    language: this.props.i18n.language
                });
            })
            .catch(err => console.log(`There was an error:${  err}`));
    }

    render() {
        const { index, limiter, articles } = this.state;
        const { category } = this.props;
        return (
            <Row className='category-list-container' noGutters>
                <Col className="my-auto list-control">
                    <Button variant='link'
                            onClick={() => this.handlePrev(index, limiter) }
                            disabled={index === 0}>
                        &lt;</Button>
                </Col>
                <Col className='items-container'>
                    <CategoryTitle title={category}/>
                    <div className='item-list'>
                        { articles.slice(index, limiter).map((article, i) =>
                            <Article key={article.url} article={article}/>
                        )}
                    </div>
                </Col>
                <Col className="my-auto list-control">
                    <Button variant='link'
                            onClick={() => this.handleNext(index, limiter) }
                            disabled={limiter === articles.length}>
                        &gt;</Button>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

export default withTranslation('common')(connect(mapStateToProps)(CategoryList));
