import React, {Component} from 'react';
import {connect} from "react-redux";
// Translation
import {withTranslation} from "react-i18next";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Form from 'react-bootstrap/Form';

import ArticlePage from "./ArticlePage";
import Article from "./Article";
import PageTitle from "./PageTitle";
import Col from "react-bootstrap/Col";

class ArticleList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            filteredArticles: [],
            language: '',
            searchText: '',
        };

        this.searchArticles = this.searchArticles.bind(this);
    }

    // On mount get articles
    componentDidMount() {
        this.getArticles();
    }

    // On change
    componentDidUpdate() {
        if (this.state.language !== this.props.i18n.language) {
           this.getArticles();
        }
    }

    // Get articles
    getArticles() {
        const { selectedCategory } = this.props;
        let API_URL;
        const BASE_URL = process.env.REACT_APP_BASE_URL;
        const API_KEY = process.env.REACT_APP_API_KEY;
        const CATEGORY = selectedCategory.category !== null ? '&category=' + selectedCategory.category : '';
        API_URL = BASE_URL + 'top-headlines?country=' + this.props.i18n.language + CATEGORY + API_KEY;
        fetch(API_URL)
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    articles: data.articles,
                    language: this.props.i18n.language,
                    searchText: ''
                });
            })
            .catch(err => console.log(`There was an error:${  err}`));
    }

    // Search articles
    searchArticles(e) {
        if (e.key === 'Enter') {
            this.setState({searchText: e.target.value.toLowerCase()})
            e.preventDefault();
        }
    }

    render() {
        const { selectedArticle, selectedCategory, t } = this.props;
        const { articles, searchText } = this.state;
        const titleText = selectedCategory.category !== null ?
            `${t('pageTitles.top')  } ` + selectedCategory.category + ` ${t('pageTitles.news-from')}`
            : `${t('pageTitles.top')  } ${t('pageTitles.news-from')}`;
        if(selectedArticle.article) {
            return (
                <Container fluid="md">
                    <ArticlePage/>
                </Container>
            )
        }

        return (
            <Container fluid="md">
                <PageTitle title={titleText}/>
                {
                    // Search input for filtering articles
                    window.location.pathname.split("/").pop() === 'search' &&
                    <Row>
                        <Col xl={12}>
                            <Form style={{margin: "1rem auto 3rem auto", width: "50%"}}>
                                <Form.Control type="text"
                                              placeholder="Search term"
                                              onKeyDown={this.searchArticles}
                                />
                            </Form>
                        </Col>
                    </Row>
                }

                <Row md={3} sm={2} xs={1}>
                    {
                        articles.filter(article =>
                            article.title.toLowerCase().includes(searchText.toLowerCase())
                        ).map((article) =>
                            <Article key={ article.url } article={article}/>
                        )
                    }
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

export default withTranslation('common')(connect(mapStateToProps)(ArticleList));
