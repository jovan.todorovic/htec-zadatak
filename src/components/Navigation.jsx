import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavItem from "react-bootstrap/NavItem";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {connect} from "react-redux";

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.switchLanguage = this.switchLanguage.bind(this);
    }

    switchLanguage(lang) {
        this.props.i18n.changeLanguage(lang);
    }

    render() {
        const { t, i18n, selectedArticle } = this.props;
        return (
            <Navbar bg="light">
                <Nav className="mr-auto">
                    <NavItem eventkey={1} href="/">
                        <Nav.Link as={NavLink} to="/headlines">
                            {t('navbar.link.top-news')}
                        </Nav.Link>
                    </NavItem>
                    <NavItem eventkey={1} href="/">
                        <Nav.Link as={NavLink} to="/categories">
                            {t('navbar.link.categories')}
                        </Nav.Link>
                    </NavItem>
                    <NavItem eventkey={1} href="/">
                        <Nav.Link as={NavLink} to="/search">
                            {t('navbar.link.search')}
                        </Nav.Link>
                    </NavItem>
                </Nav>
                <Form inline className="justify-content-end">
                    <Button variant={
                        i18n.language === 'gb' ? 'primary' : 'link'
                    } onClick={() => this.switchLanguage('gb')}
                      disabled={selectedArticle.article}
                    > {t('navbar.button.gb')}</Button>
                    <Button variant={
                        i18n.language === 'us' ? 'primary' : 'link'
                    }  onClick={() => this.switchLanguage('us')}
                       disabled={selectedArticle.article}
                    > {t('navbar.button.us')}</Button>
                </Form>
            </Navbar>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

export default withTranslation('common')(connect(mapStateToProps)(Navigation));
