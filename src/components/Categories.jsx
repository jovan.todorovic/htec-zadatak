import React, {Component} from 'react';
// Translation
import {withTranslation} from "react-i18next";
import Title from "./PageTitle";
import {connect} from "react-redux";
import {deselectCategoryAction} from "../store/actions/categoryAction";
import Container from "react-bootstrap/Container";
import CategoryList from "./CategoryList";
import './Categories.sass';
import ArticlePage from "./ArticlePage";
import ArticleList from "./ArticleList";


class Categories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [
                'entertainment',
                'general',
                'health',
                'science',
                'sports',
                'technology',
            ]
        }
    }

    componentWillUnmount() {
        this.props.deselectCategoryAction();
    }

    render() {
        const { categories } = this.state;
        const { t, selectedArticle, selectedCategory } = this.props;
        if (selectedArticle.article) {
            return (
                <Container fluid="sm">
                    <ArticlePage/>
                </Container>
            );
        } if (selectedCategory.category) {
            return (
                <Container fluid="md">
                    <ArticleList/>
                </Container>
            );
        }
        return (
            <Container className='categories-page' fluid>
                <Title title={t('pageTitles.top-news-categories')}/>
                <div className='category-list'>
                    {
                        categories.map((category, i) =>
                            <CategoryList key={ i } category={category}/>
                        )
                    }
                </div>
            </Container>
        );
    }
}
const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    deselectCategoryAction: () => dispatch(deselectCategoryAction())
});

export default withTranslation('common')(connect(mapStateToProps, mapDispatchToProps)(Categories));
