import React from 'react';
import {useTranslation} from "react-i18next";
import Button from "react-bootstrap/Button";
import {connect} from "react-redux";
import {selectArticleAction} from "../store/actions/articleAction";
import './Article.sass';

function Article(props) {
    const { t } = useTranslation('common');
    return (
        <div className='article-item'>
            <div className='article-content'>
                <h3>{props.article.title}</h3>
                <img src={props.article.urlToImage} alt={t('article.error.no-img')}/>
                <p>{props.article.description}</p>
                <Button variant='link' onClick={() => props.selectArticleAction(props.article)}>{t('article.button.more')} &gt;</Button>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    selectArticleAction: (article) => dispatch(selectArticleAction(article))
});

export default connect(mapStateToProps, mapDispatchToProps)(Article);
