import React from 'react';
import styled from 'styled-components';
import './CategoryTitle.sass';
import {selectCategoryAction} from "../store/actions/categoryAction";
import {connect} from "react-redux";

const Dot = styled.span`
        height: 10px;
        width: 10px;
        margin: 5px;
        background-color: black;
        border-radius: 50%;
        display: inline-block;
    `;

const ExpandIcon = styled.span`
        margin-left: 10px;
        position: relative;
        bottom: -2px;
        display: inline-block;
        transform: rotate(-90deg)
    `;

function CategoryTitle(props) {
    return (
        <h3 className='category-title'>
            <Dot/>
            <span style={{textDecoration:"underline"}} onClick={() => props.selectCategoryAction(props.title)}>{props.title}</span>
            <ExpandIcon>&lt;</ExpandIcon>
        </h3>
    )
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    selectCategoryAction: (category) => dispatch(selectCategoryAction(category))
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryTitle);
