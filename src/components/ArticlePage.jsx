import React, {Component} from 'react';
// Translation
import {withTranslation} from "react-i18next";
import styled from 'styled-components';
import {connect} from "react-redux";
import {deselectArticleAction} from "../store/actions/articleAction";
// React components
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import PageTile from "./PageTitle";

class ArticlePage extends Component {
    componentWillUnmount() {
        this.props.deselectArticleAction();
    }

    render() {
        const { t, selectedArticle, deselectArticleAction } = this.props;
        const Image = styled.div`
          display: block;
          height: 250px;
          width: 100%;
          background-image:  url('${props => props.imgUrl}');;
          background-repeat: no-repeat;
          background-size: 100% auto;
          background-position: center;
          margin-bottom: 20px;
        `;
        return (
            <Container className='article-page'>
                <Row>
                    <PageTile title={selectedArticle.article.title}/>
                </Row>
                <Row>
                    <Image imgUrl={selectedArticle.article.urlToImage}/>
                    <div className='content'>
                        <p>{selectedArticle.article.description}</p>
                    </div>

                </Row>
                <Row>
                    <Button variant='link' onClick={() => deselectArticleAction() }>
                        &lt; {t('articlePage.button.go-back')}</Button>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    deselectArticleAction: () => dispatch(deselectArticleAction())
});

export default withTranslation('common')(connect(mapStateToProps, mapDispatchToProps)(ArticlePage));
