import React from 'react';
// React router
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from 'react-router-dom';
import './App.sass';
import Navigation from "./components/Navigation";
import ArticleList from "./components/ArticleList";
import Categories from "./components/Categories";

function App() {
  return (
      <Router>
        <div className="App">
          <Navigation/>
          <div className="app-container">
            <Switch>
              <Route exact path="/">
                <Redirect to="/headlines"/>
              </Route>
              <Route path="/headlines" component={ArticleList}/>
              <Route path="/categories" component={Categories}/>
              <Route path="/search" component={ArticleList} />
            </Switch>
          </div>
        </div>
      </Router>
  );
}

export default App;
