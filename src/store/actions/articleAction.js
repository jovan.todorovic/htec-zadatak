export const selectArticleAction = (article) => {
    return {
        type: "setArticle",
        article: article
    }
}

export const deselectArticleAction = () => {
    return {
        type: "setArticle",
        article: null
    }
}
