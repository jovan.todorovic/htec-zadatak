export const selectCategoryAction = (category) => {
    return {
        type: "setCategory",
        category: category
    }
}

export const deselectCategoryAction = () => {
    return {
        type: "setCategory",
        category: null
    }
}
