import { createStore } from "redux";
import rootReducer from './reducers/rootReducer';

function store(state = {
    selectedArticle: {
        article: null
    },
    selectedCategory: {
        category: null
    },
}) {
    return createStore(
        rootReducer,
        state,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
}

export default store;
