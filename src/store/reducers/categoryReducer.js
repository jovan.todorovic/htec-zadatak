export default (state = {}, action) => {
    switch (action.type) {
        case "setCategory":
            return {
                ...state,
                category: action.category
            };
        default:
            return state;
    }
};
