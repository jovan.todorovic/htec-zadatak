export default (state = {}, action) => {
    switch (action.type) {
        case "setArticle":
            return {
                ...state,
                article: action.article
            };
        default:
            return state;
    }
};
